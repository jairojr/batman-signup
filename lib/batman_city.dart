import 'package:flutter/material.dart';

class BatmanCity extends AnimatedWidget {
  const BatmanCity({Key key, Animation animation})
      : super(key: key, listenable: animation);

  Animation get _animationCity => listenable as Animation;

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: _CityCliper(progress: _animationCity.value),
      child: Image.asset(
        'images/city.png',
        fit: BoxFit.contain,
      ),
    );
  }
}

class _CityCliper extends CustomClipper<Path> {
  final double progress;

  _CityCliper({this.progress});

  @override
  Path getClip(Size size) {
    // TODO: implement getClip

    final path = Path();
    path.moveTo(0.0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width / 2, size.height * (1 - progress));
    path.lineTo(0.0, size.height);

    return path;
    // throw UnimplementedError();
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper)  => true;
}
