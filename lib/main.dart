import 'package:batman_sign_up/main_batman_sign_up_app.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Batman sign up',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainBatmanSignUpApp(),
    );
  }
}
