import 'package:flutter/material.dart';
import 'batman_button.dart';

class BatmanScreenButtons extends AnimatedWidget {

  final VoidCallback onTap;

  const BatmanScreenButtons({Key key, Animation animation, this.onTap})
      : super(key: key, listenable: animation);

  Animation get _animationButtonIn => listenable as Animation;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0),
      child: Opacity(
        opacity: _animationButtonIn.value,
        child: Transform.translate(
          offset: Offset(0.0, 100 * (1 - _animationButtonIn.value)),
          child: Column(
            children: [
              BatmanButton(
                text: 'LOGIN',
                left: false,
              ),
              SizedBox(height: 15),
              BatmanButton(
                text: 'SIGNUP',
                onTap: onTap,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
